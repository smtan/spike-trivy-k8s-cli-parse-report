# Purpose
This code is a spike to explore the challenges faced when using [trivy k8s](https://aquasecurity.github.io/trivy/v0.38/docs/target/kubernetes/) to run a scan for the workloads in a cluster.

# Prereq
- Install [k3d](https://github.com/k3d-io/k3d#get)
- Start a cluster that contains a running pod eg nginx
- The code reads the kube config from ~/.kube/config

# Run
- `go run main.go`
- This could take some time since there's a need to download the trivy db

# Expectation
If the scan completes successfully, you should see outputs like
```
Cluster name: 
Number of findings: 10

Findings for Namespace: kube-system, Kind: Deployment, Name: coredns
Number of results: 1

Findings for Namespace: default, Kind: Deployment, Name: nginx
Number of results: 1
```