package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

type ScanResult struct {
	ClusterName string    `json:"ClusterName"`
	Findings    []Finding `json:"Findings"`
}

type Finding struct {
	Namespace string       `json:"Namespace"`
	Kind      string       `json:"Kind"`
	Name      string       `json:"Name"`
	Results   []ResultItem `json:"Results"`
}

type ResultItem struct {
	Target          string          `json:"Target"`
	Class           string          `json:"Class"`
	Type            string          `json:"Type"`
	Vulnerabilities []Vulnerability `json:"Vulnerabilities"`
}

type Vulnerability struct {
	VulnerabilityID  string            `json:"VulnerabilityID"`
	PkgName          string            `json:"PkgName"`
	InstalledVersion string            `json:"InstalledVersion"`
	FixedVersion     string            `json:"FixedVersion"`
	Layer            Layer             `json:"Layer"`
	SeveritySource   string            `json:"SeveritySource"`
	PrimaryURL       string            `json:"PrimaryURL"`
	Description      string            `json:"Description"`
	Severity         string            `json:"Severity"`
	CweIDs           []string          `json:"CweIDs"`
	CVSS             map[string]CVSS   `json:"CVSS"`
	References       []string          `json:"References"`
	PublishedDate    string            `json:"PublishedDate"`
	LastModifiedDate string            `json:"LastModifiedDate"`
	DataSource       VulnerabilityData `json:"DataSource"`
}

type Layer struct {
	Digest string `json:"Digest"`
	DiffID string `json:"DiffID"`
}

type CVSS struct {
	V3Vector string  `json:"V3Vector"`
	V3Score  float64 `json:"V3Score"`
	V2Vector string  `json:"V2Vector"`
	V2Score  float64 `json:"V2Score"`
}

type VulnerabilityData struct {
	ID   string `json:"ID"`
	Name string `json:"Name"`
	URL  string `json:"URL"`
}

func main() {
	var kubeconfig string

	// Set up the Kubeconfig path
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = filepath.Join(home, ".kube", "config")
	} else {
		kubeconfig = ""
	}

	// Load the kubeconfig file
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// Create the Kubernetes client
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	namespace := "default"
	podName := "trivy-scanner"

	// Define pod spec
	pod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: podName,
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  "trivy",
					Image: "aquasec/trivy:0.38.3",
					Command: []string{
						"trivy",
						"k8s",
						"--no-progress",
						"--quiet",
						"--report=summary",
						"cluster",
						"--scanners=vuln",
						"--db-repository",
						"registry.gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad",
						"--namespace",
						namespace,
						"--format",
						"json",
					},
				},
			},
			RestartPolicy: corev1.RestartPolicyNever,
		},
	}

	// Create pod
	_, err = clientset.CoreV1().Pods(namespace).Create(context.TODO(), pod, metav1.CreateOptions{})
	if err != nil {
		panic(err.Error())
	}

	watcher, err := clientset.CoreV1().Pods(namespace).Watch(context.TODO(), metav1.ListOptions{
		FieldSelector: fmt.Sprintf("metadata.name=%s", podName),
	})
	if err != nil {
		panic(err.Error())
	}

	for event := range watcher.ResultChan() {
		switch event.Type {
		case watch.Modified:
			pod, ok := event.Object.(*corev1.Pod)
			if ok && pod.Status.Phase == corev1.PodSucceeded {
				watcher.Stop()

				req := clientset.CoreV1().Pods(namespace).GetLogs(podName, &corev1.PodLogOptions{})
				podLogs, err := req.Stream(context.Background())
				if err != nil {
					panic(err.Error())
				}
				defer podLogs.Close()

				fileContents, err := ioutil.ReadAll(podLogs)
				if err != nil {
					panic(err.Error())
				}
				logOutput := string(fileContents)

				// Extract the JSON object from the log output
				jsonStart := strings.Index(logOutput, "{")
				if jsonStart < 0 {
					panic("Could not find JSON object in log output")
				}
				jsonEnd := strings.LastIndex(logOutput, "}")
				if jsonEnd < 0 {
					panic("Could not find JSON object in log output")
				}
				jsonStr := logOutput[jsonStart : jsonEnd+1]

				// Unmarshal the JSON object into a Go struct
				var scanResult ScanResult
				err = json.Unmarshal([]byte(jsonStr), &scanResult)
				if err != nil {
					panic(err)
				}

				fmt.Printf("Cluster name: %s\n", scanResult.ClusterName)
				fmt.Printf("Number of findings: %d\n", len(scanResult.Findings))
				for _, finding := range scanResult.Findings {
					fmt.Printf("\nFindings for Namespace: %s, Kind: %s, Name: %s\n", finding.Namespace, finding.Kind, finding.Name)
					fmt.Printf("Number of results: %d\n", len(finding.Results))
				}

				// Delete Pod
				err = clientset.CoreV1().Pods(namespace).Delete(context.TODO(), podName, metav1.DeleteOptions{})
				if err != nil {
					panic(err.Error())
				}
			}
		}
	}
}
